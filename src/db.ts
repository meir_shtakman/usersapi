import fsp from 'fs/promises';
import fs from 'fs'



export interface IUser {
    firstName: string;
    lastName: string;
    email: string;
    phoneNumber: string;
    id:string;
}

export type UserDetailsForUpdate = Partial<IUser>;

export let users: IUser[] = [];

export function saveData():void  {
    fsp.writeFile('../data.json', JSON.stringify(users));
}

export async function readData(): Promise<void> {
    if (fs.existsSync('../data.json')) {
        const data = await fsp.readFile('../data.json', "utf8");
        users = JSON.parse(data);
    } else {
        saveData();
    }
}


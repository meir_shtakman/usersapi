import { Router, RequestHandler } from "express";
import { nextTick } from "process";
import { HttpError, httpResponseMessage } from "./types.js";
import * as userHandler from "./usersHandler.js"

const route = Router();
route.get('/all', (req, res, next) => {
    try {
        const resMessage: httpResponseMessage = {
            status: 200,
            message: "Get all users request complete",
            data: userHandler.getAllUsers()
        }

        res.status(200).json(resMessage);
    } catch (err) {
        next(new HttpError("Internal error", 500));
    }
})

route.get('/:id', (req, res, next) => {
    try {
        const resMessage: httpResponseMessage = {
            status: 200,
            message: "Get user request complete",
            data: userHandler.getUserById(req.params.id)
        }

        res.status(200).send(resMessage)
    } catch (err) {
        if (!(err as HttpError).statusCode) {
            next(new HttpError("Internal error", 500));
        }
        next(err);
    }
})

route.post('/add', async (req, res, next) => {
    try {
        const resMessage: httpResponseMessage = {
            status: 201,
            message: "Create new user request complete",
            data: `user id = ${await userHandler.addUser(req.body.firstName, req.body.lastName, req.body.email, req.body.phoneNumber)}`
        }
        res.status(201).send(resMessage)
    } catch (err) {
        if (!(err as HttpError).statusCode) {
            next(new HttpError("Internal error", 500));
        }
        next(err);
    }
})

route.put('/change/:id', async (req, res, next) => {
    try {
        const resMessage: httpResponseMessage = {
            status: 204,
            message: "Update user details request complete",
            data: ""
        }
        req.body.id = req.params.id;
        await userHandler.changeUserDetails(req.body);
        res.status(204).send(resMessage)
    } catch (err) {
        if (!(err as HttpError).statusCode) {
            next(new HttpError("Internal error", 500));
        }
        next(err);
    }
})

route.delete('/remove/:id', async (req, res, next) => {
    try {
        const resMessage: httpResponseMessage = {
            status: 204,
            message: "Delete user request complete",
            data: ""
        }
        await userHandler.deleteUser(req.params.id)
        res.status(204).send('Greeting to all Users')
    } catch (err) {
        if (!(err as HttpError).statusCode) {
            next(new HttpError("Internal error", 500));
        }
        next(err);
    }
})

export default route;
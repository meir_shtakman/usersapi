import express, { NextFunction ,RequestHandler } from 'express';
import log from '@ajar/marker';
import morgan from 'morgan';
import usersRoute from './usersRoute.js'
import * as logger from './logger.js'
import * as db from './db.js';
import {addErrorLog,sendErrorMessage} from './errorHandler.js'
import { HttpError } from './types.js';

db.readData();
const { PORT, HOST } = process.env;

const app = express()
app.use(express.json())
app.use(morgan('dev'))
app.use((req, res,next) => {
    logger.addLog(`${req.method} ${req.path } ${Date.now()}
    `)
    next();
})

app.use('/users',usersRoute);

app.use("*",(req, res,next) => {
    next(new HttpError("Error 404 not found route",404));
})

app.use(addErrorLog);
app.use(sendErrorMessage);

app.listen(Number(PORT), String(HOST),  ()=> {
    log.magenta(`🌎  listening on`,`http://${HOST}:${PORT}`);
});

//------------------------------------------
//         Express Echo Server
//------------------------------------------
/* challenge instructions

     - install another middleware - morgan
        configuring app middleware like so:
        app.use( morgan('dev') );

    -  define more routing functions that use

        - req.query - access the querystring part of the request url
        - req.params - access dynamic parts of the url
        - req.body - access the request body of a POST request
        
        in each routing function you want to pass some values to the server from the client
        and echo those back in the server response

    - return api json response
    - return html markup response

    - return 404 status with a custom response to unsupported routes


*/

import * as db from './db.js';
import { UserDetailsForUpdate, IUser, HttpError, IIndexObject } from './types.js'

export function getUserById(id: string): IUser {
    const user = db.users.find(user => user.id === id);
    if (!user) {
        throw new HttpError('user not found', 400);
    }
    return user;
}

export function getAllUsers(): IUser[] {
    return db.users;
}

export async function addUser(firstName: string, lastName: string, email: string, phoneNumber: string) {
    checkIfInputMissing({ firstName, lastName, email, phoneNumber });
    const id = Math.random().toString().substr(2);
    db.users.push({ firstName, lastName, email, phoneNumber, id })
    db.saveData();
    return id;
}

function checkIfInputMissing(input: IIndexObject) {
    for (const [key, value] of Object.entries(input)) {
        if (!value) {
            throw new HttpError(`request pramater is missing: ${key}`, 400);
        }
    }
}

export async function changeUserDetails(userDetails: UserDetailsForUpdate) {
    const id:string|undefined = userDetails.id;
    checkIfInputMissing({id})
    const user = getUserById(String(id));
    user.firstName = userDetails.firstName ? userDetails.firstName : user.firstName;
    user.lastName = userDetails.lastName ? userDetails.lastName : user.lastName;
    user.email = userDetails.email ? userDetails.email : user.email;
    user.phoneNumber = userDetails.phoneNumber ? userDetails.phoneNumber : user.phoneNumber;
    db.saveData();
}

export async function deleteUser(id: string) {
    checkIfInputMissing({id})
    const index = db.users.findIndex(user => user.id === id);
    if (index === -1) {
        throw new HttpError('user not found', 400);
    } else {
        db.users.splice(index);
    }
    db.saveData();
}


import express, { NextFunction ,Request,Response } from 'express';
import * as logger from './logger.js';
import { HttpError, httpResponseMessage } from './types.js';
export const addErrorLog = (error: HttpError, req: Request, res: Response, next: NextFunction) => {
    logger.addErrorLog(error.toString());
    next(error);
}

export const sendErrorMessage = (error: HttpError, req: Request, res: Response, next: NextFunction) => {
    const resMessage:httpResponseMessage ={
        status: error.statusCode,
        message: error.message,
        data: "request failed"
    }
    res.status(error.statusCode).send(resMessage);
}